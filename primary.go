package weaponstats

//Primary stats are displayed in game
type Primary struct {
	Impact          int
	Range           int
	Stability       int
	ReloadSpeed     int
	Handling        int
	Magazine        int
	RoundsPerMinute int
}
