package weaponstats

// Hidden stats stats are not displayed in game
type Hidden struct {
	Zoom            int
	InventorySize   int
	AimAssistance   int
	RecoilDirection int
}
